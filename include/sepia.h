#ifndef SEPIA
#define SEPIA
#include "../include/image.h"
struct image* sepia_c(struct image* source);
void sepia_asm(struct image* source);
#endif
