.PHONY: clean

build:
	nasm -f elf64 ./src/sepia_filter.asm -o sepia_filter.o
	gcc -no-pie ./include/*.h ./src/*c ./sepia_filter.o -o filter

build_optimized:
	nasm -f elf64 ./src/sepia_filter.asm -o sepia_filter.o
	gcc -O2 -no-pie ./include/*.h ./src/*c ./sepia_filter.o -o filter

clean:
	rm *.o