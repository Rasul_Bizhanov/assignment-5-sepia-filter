#include "../include/io.h"
#include "../include/rotate.h"
#include "../include/sepia.h"
#include <time.h>

#define NUM_ITERATIONS 100

int main(int argc, char* argv[]) {
    if (argc != 4) {
        printf("Usage: %s <source - image> <transformed using c - image> <transformed using asm - image>\n", argv[0]);
        return 1;
    }

    char* input_filename = argv[1];
    char* output_filename_c = argv[2];
    char* output_filename_asm = argv[3];
    struct image* original_image;
    struct image* sepia_image;
    float total_time_1 = 0.0;
    float total_time_2 = 0.0;

    for (int i = 0; i < NUM_ITERATIONS; ++i) {
        enum read_status read_result = read(input_filename, &original_image);

        if (read_result != READ_OK) {
            printf("Error reading the input file. Status: %d\n", read_result);
            return 1;
        }
        clock_t start_1 = clock();
        sepia_image = sepia_c(original_image);
        clock_t end_1 = clock();
        total_time_1 += (float)(end_1 - start_1);

        clock_t start_2 = clock();
        sepia_asm(original_image);
        clock_t end_2 = clock();
        total_time_2 += (float)(end_2 - start_2);
    }

    float avg_time_1 = total_time_1 / NUM_ITERATIONS;
    float avg_time_2 = total_time_2 / NUM_ITERATIONS;

    enum write_status write_result_c = write(output_filename_c, sepia_image);
    if (write_result_c != WRITE_OK) {
        printf("Error writing to the output file. Status: %d\n", write_result_c);
        return 1;
    }

    enum write_status write_result_asm = write(output_filename_asm, original_image);
    if (write_result_asm != WRITE_OK) {
        printf("Error writing to the output file. Status: %d\n", write_result_asm);
        return 1;
    }

    printf("C: %f \n", avg_time_1);
    printf("ASM: %f \n", avg_time_2);

    return 0;
}
