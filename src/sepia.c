#include "../include/sepia.h"
extern void sepia_filter(struct pixel* img);
static struct pixel sepia_pixel(struct pixel pixel) {
    struct pixel output_pixel;
    float inputR = pixel.r;
    float inputG = pixel.g;
    float inputB = pixel.b;

    float outputR = (inputR * 0.393f) + (inputG * 0.769f) + (inputB * 0.189f);
    outputR = (outputR > 255) * 255 + (outputR <= 255) * outputR;

    float outputG = (inputR * 0.349f) + (inputG * 0.686f) + (inputB * 0.168f);
    outputG = (outputG > 255) * 255 + (outputG <= 255) * outputG;

    float outputB = (inputR * 0.272f) + (inputG * 0.534f) + (inputB * 0.131f);
    outputB = (outputB > 255) * 255 + (outputB <= 255) * outputB;

    output_pixel.r = (uint8_t)outputR;
    output_pixel.g = (uint8_t)outputG;
    output_pixel.b = (uint8_t)outputB;
    return output_pixel;
}

struct image* sepia_c(struct image* source) {
    struct image* output_image = create_image(source->width, source->height);

    for (size_t y = 0; y < output_image->height; ++y) {
        for (size_t x = 0; x < output_image->width; ++x) {
            set_pixel(output_image, x, y, sepia_pixel(source->data[y * source->width + x]));
        }
    }
    return output_image;
}
 void sepia_asm(struct image* image) {
     for (size_t y = 0; y < image->height; ++y) {
         for (size_t x = 0; x < image->width; ++x) {
             sepia_filter(&(image->data[y*image->width+x]));
         }
     }
}
