%macro convert_byte_to_float 2
    xor rax, rax
    mov al, byte %2
    sar rax, 7
    mov al, byte %2
    movd %1, eax
    shufps %1, %1, 0
    cvtdq2ps %1, %1
%endmacro

%macro apply_sepia_filter 3
    movups xmm3, [%1]
    movups xmm4, [%2]
    movups xmm5, [%3]

    mov rsi, [rdi + 3]

    convert_byte_to_float xmm0, [rdi]
    convert_byte_to_float xmm1, [rdi + 1]
    convert_byte_to_float xmm2, [rdi + 2]

    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5

    addps xmm0, xmm1
    addps xmm0, xmm2

    cvtps2dq xmm0, xmm0
    packusdw xmm0, xmm0
    packuswb xmm0, xmm0

    movd [rdi], xmm0
    mov [rdi + 3], rsi
%endmacro

section .rodata
align 16
a: dd 0.131, 0.168, 0.189
align 16
b: dd 0.534, 0.686, 0.769
align 16
c: dd 0.272, 0.346, 0.387

section .text
;rdi - pixel*
global sepia_filter
sepia_filter:
    apply_sepia_filter a, b, c
    ret
